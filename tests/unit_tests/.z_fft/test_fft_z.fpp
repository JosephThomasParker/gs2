
!> A program that tests the nonlinear terms module. It incorporates ffttest,
!! an earlier program  which initialises the distribution
!! function and fields with specific functions and sees if the poisson bracket
!! is correctly calculated.
!!
!! This is free software released under the MIT license
!!   Written by: Joseph Parker (joseph.parker@stfc.ac.uk)

program test_fft_z
  use unit_tests
  use mp, only: init_mp, finish_mp, proc0, broadcast
  use file_utils, only: init_file_utils, run_name
  use species, only: init_species, nspec, spec
  use dist_fn, only: init_dist_fn, finish_dist_fn
  use constants, only: pi
  use kt_grids, only: naky, ntheta0, init_kt_grids
  use theta_grid, only: ntgrid, init_theta_grid
  use gs2_layouts, only: init_gs2_layouts, g_lo, ie_idx
  use nonlinear_terms, only: init_nonlinear_terms, finish_nonlinear_terms
  use dist_fn_arrays, only: g
  use nonlinear_terms, only: nonlinear_terms_unit_test_time_add_nl
  use kt_grids, only: ntheta0, naky
  use gs2_transforms, only: init_zf
#ifdef MPI
  use mpi
#endif
  implicit none
  real :: eps
    character (500), target :: cbuff
  real :: tstart
  logical :: dummy=.false.
  integer, dimension(:), allocatable :: sizes
  real, dimension(:,:,:), allocatable :: energy_results
  real :: energy_min
  real :: vcut_local
  integer :: i

  complex, dimension (:,:,:), allocatable :: integrate_species_results
  complex, dimension (:,:,:), allocatable :: g1
  complex, dimension (:,:,:), allocatable :: phi, apar, bpar


  ! General config
  eps = 1.0e-7

  ! Set up depenencies
  call init_mp
  if (proc0) call init_file_utils(dummy, name="gs")
       if (proc0) then
          cbuff = trim(run_name)
       end if
       
       call broadcast (cbuff)
       if (.not. proc0) run_name => cbuff

  call announce_module_test('fft_z')


  call init_dist_fn

  call announce_test('run nonlinear terms')
  call process_test(test_ffts(),'fast fourier transforms in nonlinear term')


  call finish_dist_fn

  call close_module_test('nonlinear_terms')

  call finish_mp


contains

function test_ffts()
  use unit_tests
  use kt_grids, only: ntheta0, naky
  use gs2_transforms, only: kz_spectrum, init_zf
  use theta_grid, only: ntgrid
#ifdef MPI
  use mpi
#endif
  implicit none
  logical :: test_ffts
  logical ::dummy
  integer :: ix, iy
  character(len=22) :: message = ''
  complex, dimension (:,:,:), allocatable :: phiin, phiout
  
  dummy = .true.

  call init_zf (ntgrid, ntheta0*naky)

  allocate(phiin(-ntgrid:ntgrid,ntheta0,naky))  
  allocate(phiout(-ntgrid:ntgrid,ntheta0,naky))  

  phiin = 0.
  phiin(-ntgrid,1,1) = 1.
  write(*,*) phiin(:,1,1)
  call kz_spectrum(phiin,phiout)
  write(*,*) phiout(:,1,1)

  test_ffts = dummy
  stop

end function test_ffts


end program test_fft_z
